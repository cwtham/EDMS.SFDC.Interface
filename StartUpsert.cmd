@echo off
setlocal EnableDelayedExpansion
REM /* Override default JAVA_HOME.
REM /* Set to JDK version that is supporting Data Loader .jar file.
set JAVA_HOME=D:\jdk1.8.0_172

REM /* Preset variables
set err=N

call:readParameters %*
if "%errorlevel%" neq "0" (
	call:endgame
	exit /b 1
)
call:prepVariables
REM echo Upsert configurations for {%env%}:
REM echo Operation log      : "%logfile%"
REM echo CSV generation log : "%logcsv%"
REM echo Upsert config path : "%confpath%"
REM echo Mapping config path: "%mappath%"
REM echo CSV source path    : "%csvsrcpath%"
REM echo Logs path          : "%logpath%"
REM echo CSV target path    : "%csvtrgpath%"
REM echo Data Loader app    : "%dataloader%"
call:validateLocations
if "%errorlevel%" neq "0" (
	call:endgame
	exit /b 1
)
call:startProcess
if "%errorlevel%" equ "1" (
	REM /* CsvOutput folder not found error.

)
if "%errorlevel%" equ "2" (
	REM /* other kind of error
)
REM /* call endgame to endlocal
call:endgame
exit /b 0

:readParameters
	REM /* Pre-set flags to all Y
	set upDea=Y
	set upPro=Y
	set upAcc=Y
	set upVeh=Y
	set upSal=Y
	REM /* capture {environment}.
	if "%~1" neq "" (
		set env=%~1
	) else (
		call:help "Environment is required."
		exit /b 1
	)
	REM /* capture <CSV maker folder>.
	if "%~2" neq "" (
		set csv=%~2
	) else (
		call:help "CSV generator folder is required."
		exit /b 1
	)
	REM /* capture Dealer flag - default Y
	if "%~3" neq "" if /i "%~3" equ "N" (set upDea=N)
	REM /* capture Product flag - default Y
	if "%~4" neq "" if /i "%~4" equ "N" (set upPro=N)
	REM /* capture Account flag - default Y
	if "%~5" neq "" if /i "%~5" equ "N" (set upAcc=N)
	REM /* capture Vehicle flag - default Y
	if "%~6" neq "" if /i "%~6" equ "N" (set upVeh=N)
	REM /* capture Sales Order flag - default Y
	if "%~7" neq "" if /i "%~7" equ "N" (set upSal=N)
	REM /* check if environment is specified correctly
	set validenv=N
	if /i "%env%" equ "sand" (set validenv=Y)
	if /i "%env%" equ "prod" (set validenv=Y)
	if /i "%validenv%" neq "Y" (
		call:error "Specified Environment: "%env%" is invalid."
		set err=Y
	)
	REM /* check path exists
	if not exist "%csv%\" (
		call:error "Specified CSV Maker folder "%csv%" not found."
		set err=Y
		REM exit /b 1
	)
	if /i "%err%" equ "Y" (exit /b 1)
	exit /b 0

:prepVariables
	REM /* configure variables, then perform Upsert
	set confpath=%~dp0Config.%env%
	set mappath=%~dp0MapConfig
	set logfile=logs.%env%.txt
	set logcsv=csv.%env%.log
	set logpath=%~dp0Logs
	set csvsrcpath=%~dp0CsvOutput
	set csvtrgpath=%logpath%\CSV
	set dataloader=%~dp0DataLoader
	exit /b 0

:validateLocations
	REM /* verify JDK @ %JAVA_HOME% folder.
	if not exist "%JAVA_HOME%\bin\java.exe" (
		call:error "JDK at [%JAVA_HOME%] not found. Unable to proceed."
		exit /b 1
	)
	REM /* verify DataLoader folder is exists
	if not exist "%dataloader%\" (
		call:error "DataLoader folder not found. Unable to proceed."
		exit /b 1
	)
	REM /* verify Field Mapping config folder is exists.
	if not exist "%mappath%\" (
		call:error "MapConfig folder not found. Unable to proceed."
		exit /b 1
	)
	exit /b 0

:startProcess
	REM /* move to batch file's folder (%~dp0).
	pushd %~dp0
	if /i "%upDea%%upPro%%upAcc%%upVeh%%upSal%" neq "NNNNN" (
		call:writeLog "Generating CSV files from EDMS database . . ."
		REM /* chdir: CsvGenny.{env}
		pushd %csv%
		if "%upDea%" equ "Y" (
			echo.    Generating Dealer CSV . . .
			call:writeLog "Writing Dealer CSV for {%env%} environment . . ."
			call Dealer.exe >>"%logpath%\%logcsv%"
		)
		if "%upPro%" equ "Y" (
			echo.    Generating Product CSV . . .
			call:writeLog "Writing Product CSV for {%env%} environment . . ."
			call Product.exe >>"%logpath%\%logcsv%"
		)
		if "%upAcc%" equ "Y" (
			echo.    Generating Account CSV . . .
			call:writeLog "Writing Account CSV for {%env%} environment . . ."
			call Account.exe >>"%logpath%\%logcsv%"
		)
		if "%upVeh%" equ "Y" (
			echo.    Generating Vehicle CSV . . .
			call:writeLog "Writing Vehicle CSV for {%env%} environment . . ."
			call Vehicle.exe >>"%logpath%\%logcsv%"
		)
		if "%upSal%" equ "Y" (
			echo.    Generating Sales Order CSV . . .
			call:writeLog "Writing Sales Order CSV for {%env%} environment . . ."
			call SalesOrder.exe >>"%logpath%\%logcsv%"
		)
		call:writeLog "All CSV generated!"
		popd
		REM /* Verify CsvOutput folder is exists
		if not exist "%csvsrcpath%\" (
			call:error "CsvOutput folder not found. Unable to proceed."
			exit /b 1
		)
		REM /* Acquire current date/time for output file
		call:getDateTime
		if "%upDea%" equ "Y" (
			set label=Dealer
			set file=dealer
			set svc=upsertDealer.%env%
			set csvfile=!file!.%env%.csv
			set errfile=!file!.%env%.err.csv
			set upslogfile=!file!.%env%.log
			set csvsrcfile="%csvsrcpath%\!csvfile!"
			set errsrcfile="%csvsrcpath%\!errfile!"
			set csvtrgfile="%csvtrgpath%\!datetime!-!csvfile!"
			set errtrgfile="%csvtrgpath%\!datetime!-!errfile!"
			set title=Running Job -- !label! {%env%}
			call:printCopyright "!label!"
			call:startLoader
			if "%errorlevel%" equ "0" (
				call:writeLog "!label! loading ended."
			) else (
				call:writeLog "!label! loading ended with error."
			)
		)
		if "%upPro%" equ "Y" (
			set label=Product
			set file=product
			set svc=upsertProduct.%env%
			set csvfile=!file!.%env%.csv
			set errfile=!file!.%env%.err.csv
			set upslogfile=!file!.%env%.log
			set csvsrcfile="%csvsrcpath%\!csvfile!"
			set errsrcfile="%csvsrcpath%\!errfile!"
			set csvtrgfile="%csvtrgpath%\!datetime!-!csvfile!"
			set errtrgfile="%csvtrgpath%\!datetime!-!errfile!"
			set title=Running Job -- !label! {%env%}
			call:printCopyright "!label!"
			call:startLoader
			if "%errorlevel%" equ "0" (
				call:writeLog "!label! loading ended."
			) else (
				call:writeLog "!label! loading ended with error."
			)
		)
		if "%upAcc%" equ "Y" (
			set label=Account
			set file=account
			set svc=upsertAccount.%env%
			set csvfile=!file!.%env%.csv
			set errfile=!file!.%env%.err.csv
			set upslogfile=!file!.%env%.log
			set csvsrcfile="%csvsrcpath%\!csvfile!"
			set errsrcfile="%csvsrcpath%\!errfile!"
			set csvtrgfile="%csvtrgpath%\!datetime!-!csvfile!"
			set errtrgfile="%csvtrgpath%\!datetime!-!errfile!"
			set title=Running Job -- !label! {%env%}
			call:printCopyright "!label!"
			call:startLoader
			if "%errorlevel%" equ "0" (
				call:writeLog "!label! loading ended."
			) else (
				call:writeLog "!label! loading ended with error."
			)
		)
		if "%upVeh%" equ "Y" (
			set label=Vehicle
			set file=vehicle
			set svc=upsertVehicle.%env%
			set csvfile=!file!.%env%.csv
			set errfile=!file!.%env%.err.csv
			set upslogfile=!file!.%env%.log
			set csvsrcfile="%csvsrcpath%\!csvfile!"
			set errsrcfile="%csvsrcpath%\!errfile!"
			set csvtrgfile="%csvtrgpath%\!datetime!-!csvfile!"
			set errtrgfile="%csvtrgpath%\!datetime!-!errfile!"
			set title=Running Job -- !label! {%env%}
			call:printCopyright "!label!"
			call:startLoader
			if "%errorlevel%" equ "0" (
				call:writeLog "!label! loading ended."
			) else (
				call:writeLog "!label! loading ended with error."
			)
		)
		if "%upSal%" equ "Y" (
			set label=Sales Order
			set file=salesorder
			set svc=upsertSalesOrder.%env%
			set csvfile=!file!.%env%.csv
			set errfile=!file!.%env%.err.csv
			set upslogfile=!file!.%env%.log
			set csvsrcfile="%csvsrcpath%\!csvfile!"
			set errsrcfile="%csvsrcpath%\!errfile!"
			set csvtrgfile="%csvtrgpath%\!datetime!-!csvfile!"
			set errtrgfile="%csvtrgpath%\!datetime!-!errfile!"
			set title=Running Job -- !label! {%env%}
			call:printCopyright "!label!"
			call:startLoader
			if "%errorlevel%" equ "0" (
				call:writeLog "!label! loading ended."
			) else (
				call:writeLog "!label! loading ended with error."
			)
		)
		call:writeLog "Data upload to Salesforce completed!"
		REM /* Halt 2-second before remove "status" folder 
		REM /* generated by SFDC Data Loader program.
		ping -n 2 -l 0 127.0.0.1 >nul
		rd /s/q status >nul 2>&1
	)
	REM /* return back to calling directory.
	popd
	exit /b 0

:error
	echo Error: %~1
	exit /b 0

:help
	echo Error: %~1
	echo.
	echo Usage: %~n0
	echo     {environment}    : {sand}box, {prod}uction                  ^(required^)
	echo     ^<csv generator^>  : ^<Folder to csv generators files.^>        ^(required^)
	echo     [Dealer] [Product] [Account] [Vehicle] [Sales Order]        ^(optional^)
	echo                      : [N]o to skip Upsert ^(Default: [Y]es^).
	echo.
	echo Specifying absolute paths for Sandbox environment:
	set csv=C:\CSV Load\CsvGenny.sand
	echo %~n0 sand "%csv%" Y Y N Y Y
	echo     ^<csv generator^>  : "%csv%"
	echo     [Dealer] [Product] [Account] [Vehicle] [Sales Order]
	echo        Yes       Yes      No        Yes         Yes
	echo.
	echo Specifying relative paths to %~dp0:
	set csv=CsvGenny.prod
	echo %~n0 prod "%csv%" N N Y Y Y
	echo     ^<csv generator^>  : "%~dp0DTS.prod"
	echo     [Dealer] [Product] [Account] [Vehicle] [Sales Order]
	echo        No        No       Yes       Yes         Yes
	exit /b 1

:printCopyright
	echo.    ===========================================================
	echo.        EDMS -^> SalesForce Dot Com                           
	echo.            %~1 data uploader.                               
	echo.
	echo.            Author : Cw Tham                                  
	echo.            Email  : cw.tham@hyundai.com.my                   
	echo.     --------------------------------------------------------
	echo.      Copyright (c) 2018 Hyundai-Sime Darby Motors Sdn. Bhd.  
	echo.    ===========================================================
	REM ping -n 3 -l 0 127.0.0.1 >nul
	echo.
	exit /b 0

:writeLog
	if "%~1" neq "" (
		REM /* create logpath folder if not found.
		if not exist "%logpath%\" (md "%logpath%")
		REM /* create log header if not found.
		if not exist "%logpath%\%logfile%" (echo."Date"	"Time"	"Description" >"%logpath%\%logfile%")
		REM /* write log contents.
		if exist "%logpath%\%logfile%" (
			echo."%date%"	"%time: =0%"	%* >>"%logpath%\%logfile%"
		) else (
			echo.Error writing to log: "%logpath%\%logfile%"
			ping -n 3 -l 127.0.0.1 >nul
			REM exit /b 1
		)
	)
	exit /b 0

:getDateTime
	REM /* Check WMIC is available.
	WMIC.EXE Alias /? >NUL 2>&1
	if "%errorlevel%" equ "0" (
		REM /* Use WMIC to retrieve date and time
		FOR /F "skip=1 tokens=1-6" %%G IN (
			'WMIC Path Win32_LocalTime Get Day^,Hour^,Minute^,Month^,Second^,Year /Format:table'
		) DO (
			IF "%%~L" NEQ "" (
				set _year=%%L
				set _mo=00%%J
				set _da=00%%G
				set _ho=00%%H
				set _mi=00%%I
				set _se=00%%K
			) else (
				set _month=!_mo:~-2!
				set _day=!_da:~-2!
				set _hour=!_ho:~-2!
				set _minute=!_mi:~-2!
				set _second=!_se:~-2!
				REM /* Save date/time in ISO 8601 format:
				set datetime=!_year!!_month!!_day!_!_hour!!_minute!!_second!
			)
		)
	) else (
		echo Error acquiring date/time. Unable to proceed.
		exit /b 1
	)
	exit /b 0

:startLoader
	title %title%
	REM /* chdir: DataLoader folder
	pushd "%dataloader%"
	call:writeLog "Begin uploading %label% data . . ."
	REM /* checks for source file existence
	if not exist "%csvsrcfile%" (
		call:writeLog "Source: %csvsrcfile% not found. Data load skipped!"
		popd
		exit /b 1
	) else (
		REM /* start Upsert process
		REM jdk1.8.0_172\bin\java.exe -cp .\dataloader-43.0.0-uber.jar -Dsalesforce.config.dir="%confpath%" com.salesforce.dataloader.process.ProcessRunner process.name=%svc% >"%logpath%\%upslogfile%"
		call process "%confpath%" %svc% >"%logpath%\%upslogfile%"
		REM /* move Source files to Logs folder.
		if not exist "%csvtrgpath%\" (md "%csvtrgpath%")
		call:writeLog "Moving source csv: [%csvsrcfile%] into [%csvtrgpath%] . . ."
		move "%csvsrcfile%" "%csvtrgfile%" >nul 2>&1
		call:writeLog "Moving error csv: [%errsrcfile%] into [%csvtrgpath%] . . ."
		move "%errsrcfile%" "%errtrgfile%" >nul 2>&1
		popd
		exit /b 0
	)

:endgame
	endlocal
	exit /b
For daily interface setting:
	1) Create Task Scheduler to executes:
			StartUpsert.cmd prod CsvGenny.prod
	2) Run as: local administrator
	3) Weekly: 1:00am, Every day

For selective/complete upload setting:
	1) Configure SQL query in each ".config" file under "CsvGenny.prod.all"
	2) Execute: StartUpsert.cmd prod CsvGenny.prod.all